#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h> //htons() e inet_addr()
#include <netinet/in.h> //struct sockaddr_in
#include <sys/types.h>          
#include <sys/socket.h>
#include <signal.h>
#include <ctype.h>

#include "common.h"




int sock_fd;
int res;

char risultato[1024];
char* nome;
char* password;
    
client* clientList = NULL;
head clientHead;


//gestione segnale SIGINT
void chiusura(){
	printf("\n");
	printf(ANSI_COLOR_YELLOW "Vuoi veramente uscire? " ANSI_COLOR_RESET);
	char msg[10];
	E:scanf("%s", msg);
	while('\n'!=getchar());
	if(strcmp(msg,"si")==0 || strcmp(msg, "SI") == 0){
		printf("Esco dal programma..\n");
		libera_memoria(clientHead.first);
		res = close(sock_fd);
		if(res < 0) handle_error("Errore nella chiusura della socket");
		res = remove("chat.txt");
		if(res < 0) handle_error("Errore nella eliminazione del file");
		exit(0);
	}
	else if(strcmp(msg,"no")==0 || strcmp(msg, "NO") == 0){
		printf("Ok, continuiamo\n");
	}
	else{
		printf("Devi rispondere si o no: ");
		goto E;
	}
}

void inserisci_utente(char* buf, struct sockaddr_in client_addr, char* pass){
	client* nodo = (client*) malloc(sizeof(client));
	//printf("sono nella funzione\n");	
	nodo -> username = malloc((20)*sizeof(char));
	nodo -> password = malloc((20)*sizeof(char));
	memcpy(nodo -> username, buf, 15);
	memcpy(nodo -> password, pass, 15);
	nodo->utente = client_addr;
	nodo->next = 0;
	if(clientList==NULL){
		//printf("Sto nell'if\n");
		clientList = nodo;
		//printf("L'utente è: %s\n", clientList->username);	
		clientHead.first = nodo;
	}
	else{
		//printf("Sto nell'else\n");	
		while(clientList->next != 0){
			clientList = clientList->next;
		}
		clientList->next = nodo;
	}
}

void ripeti_login(char* buf, struct sockaddr_in client_addr, struct client*head ,char* pass){
	client* c = head;
	if(strcmp(c->username, buf)==0 && strcmp(c->password, pass)==0){
		c->utente = client_addr;
		printf("L'utente ha effettuato nuovamente il login\n");
	}
	else ripeti_login(buf, client_addr, c->next, pass);
}

void stampa_lista(struct client* head){
	client* c = head;
	if(c == NULL){}
	else{
		//printf("Sono in stampa_lista, l'utente loggato è: %s\n", c->username);
		stampa_lista(c->next);
	}
	
}

void libera_memoria(struct client* head){
	client* c;
	while(head!=NULL){
		c = head;
		head = head -> next;
		free(c->username);
		free(c->password);
		free(c);
		c = NULL;
	}
}

char* lista_utenti(struct client* head){
	client* cli = head;
	
	memcpy(risultato,cli->username,15);
	cli = cli ->next;
	while(cli != NULL){
		
		strcat(risultato, ", ");
		strcat(risultato, cli->username); 
		cli = cli -> next;
	}
	return risultato;
}

/*struct sockaddr_in cerca_destinatario(struct client* head, char* dest){
	client* c = head;
	while(strcmp(c->username,dest)!=0) c=c->next;
	//printf("L'utente a cui devo inviare il messaggio è: %s\n", dest);
	//printf("L'utente a cui invio il messaggio è: %s\n", c->username);
	return c->utente;
}*/
	
int ricerca(struct client* head, char*dest, char*pass){
		client* c = head;
		if(head == NULL) return 0;
		if(strcmp(c->username,dest)==0 && strcmp(c->password,pass)==0) return 1;
		else ricerca(c->next,dest,pass);
}

int ricercaD(struct client* head, char*dest){
		client* c = head;
		if(head == NULL) return 0;
		if(strcmp(c->username,dest)==0) return 1;
		else ricercaD(c->next,dest);
}
	


int main(){
	clientHead.first = NULL;
	signal(SIGINT, chiusura);
	signal(SIGQUIT, chiusura);
    char buf[1024]; //
   // char dest[1024];
    int n;
    socklen_t len;
    size_t buf_len = sizeof(buf);
    memset(buf,0,buf_len); //setto a 0 buf
	
	FILE* f = fopen("chat.txt","w");
	fprintf(f,"inizio\n");
	fclose(f);
	
   struct sockaddr_in server_addr, client_addr; //dichiaro le strutture sockaddr_in
    
    printf(ANSI_COLOR_YELLOW "**** SERVER AVVIATO **** \n" ANSI_COLOR_RESET);
    
    //creazione socket
    sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock_fd < 0) handle_error("Errore nella creazione della socket");
    printf("Socket creata correttamente\n");
    
    memset(&server_addr, 0, sizeof(server_addr)); //inizializzo a 0 le due struct sockaddr_in
	memset(&client_addr, 0, sizeof(client_addr));
	
	
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(PORT);
	server_addr.sin_addr.s_addr = INADDR_ANY;
	
	//bind
	res = bind(sock_fd, (const struct sockaddr*)&server_addr, sizeof(server_addr));
	if (res < 0) handle_error("Errore nella bind");
	printf("Bind effettuata correttamente\n");
	
	while(1){
		
		printf(ANSI_COLOR_MAGENTA "\n*---------------------------*" ANSI_COLOR_RESET);
		
		printf("\nIn attesa di un messaggio....\n");

		len = sizeof(client_addr);
		n = recvfrom(sock_fd, (char*) buf, sizeof(buf), MSG_WAITALL, (struct sockaddr*) &client_addr, &len);
		buf[n] = '\0';
		
		char* modalita = strtok(buf, " ");
		//printf("dentro modalita c'è: %s\n",modalita);
		
		if(strcmp(modalita,"login")==0){
			
			
			nome = strtok(NULL, " ");
			password = strtok(NULL, " ");
			printf("Sta eseguendo il login: %s\n", nome);
			//printf("La password è: %s\n", password);
			
			char* risposta;
			int trovato = ricerca(clientHead.first, nome, password);
			if(trovato == 0){
				trovato = ricercaD(clientHead.first, nome);
				if(trovato == 1){
					printf("Nome gia in uso\n");
					risposta = "no";
					//printf("Dentro risposta ci sta: %s\n", risposta);
					res = sendto(sock_fd, (const char*)risposta, strlen(risposta), MSG_CONFIRM, (const struct sockaddr*) &client_addr, len);
					if(res < 0) printf("Errore nell'invio");
				}
				else{
					risposta = "ok";
					inserisci_utente(nome, client_addr, password);
					res = sendto(sock_fd, (const char*)risposta, strlen(risposta), MSG_CONFIRM, (const struct sockaddr*) &client_addr, len);
					if(res < 0) printf("Errore nell'invio");
					//printf("clientList.nome = %s\n", clientList->username);
				}
			}
			else{
				ripeti_login(nome, client_addr, clientHead.first, password);
				risposta = "ok";
				res = sendto(sock_fd, (const char*)risposta, strlen(risposta), MSG_CONFIRM, (const struct sockaddr*) &client_addr, len);
				if(res < 0) printf("Errore nell'invio");
			}
			stampa_lista(clientHead.first);
			printf(ANSI_COLOR_MAGENTA "*---------------------------*\n" ANSI_COLOR_RESET);
			
			}
		else if(strcmp(modalita,"leggo")==0){
			printf("L'utente ha richiesto la lettura dei messaggi\n");
			char* n = strtok(NULL, " ");
			char m[15];
			FILE* file = fopen("chat.txt","r");
			if(file<0) handle_error("Errore nell'apertura del file\n");
			//printf("Ho aperto il file\n");
			char d[15];
			char t[1024];
			char b[20];
			
	
			//printf("Ora entro nel while\n");
			while(fscanf(file, "%s", d)!=EOF){
				//fscanf(file, "%s", d);
				if(strcmp(d,"inizio")==0){}
				else{
					//printf("Sono dentro il while, dentro d ci sta: %s\n",d);
					int k;
					for(k=0; k<strlen(n); k++) n[k] = toupper((unsigned char)n[k]);
					//printf("Dentro n ci sta: %s\n", n);
					//printf("Il destinatario è: %s\n", d);
					fscanf(file, "%s", b);
					fscanf(file, "%s", b);
					fscanf(file, "%s", b);
					fscanf(file, "%s", m);
					fscanf(file, "%s", b);
					if(strcmp(n,d)==0) {
						//printf("Il destinatario è: %s\n", d);
						//printf("Il mittente è: %s\n", m);
						fgets(t, 200, file);
						//printf("Il messaggio è %s\n", t);
						
					
						res = sendto(sock_fd, (const char*)t, strlen(t), MSG_CONFIRM, (const struct sockaddr*) &client_addr, len);
						if(res < 0) {
							printf("Errore nell'invio del messaggio\n");
						}	
						else{
							//printf("Messaggio al client inviato\n"); 
						}
						
						char mod[5]="1";
						res = sendto(sock_fd, (const char*)mod, strlen(mod), MSG_CONFIRM, (const struct sockaddr*) &client_addr, len);
						if(res < 0) {
							printf("Errore nell'invio del messaggio\n");
						}	
						else{
							//printf("Messaggio al client inviato\n"); 
						}
						
						
						res = sendto(sock_fd, (const char*)m, strlen(m), MSG_CONFIRM, (const struct sockaddr*) &client_addr, len);
						if(res < 0) {
							printf("Errore nell'invio del messaggio\n");
						}	
						else{
							//printf("Messaggio al client inviato\n"); 
						}
					}	
				
					else if(strcmp(n,m)==0){
						//printf("Il destinatario è: %s\n", d);
						//printf("Il mittente è: %s\n", m);
						fgets(t, 200, file);
						//printf("Il messaggio è %s\n", t);
					
						res = sendto(sock_fd, (const char*)t, strlen(t), MSG_CONFIRM, (const struct sockaddr*) &client_addr, len);
						if(res < 0) {
							printf("Errore nell'invio del messaggio\n");
						}	
						else{
							//printf("Messaggio al client inviato\n"); 
						}
						
						char mod[5]="2";
						res = sendto(sock_fd, (const char*)mod, strlen(mod), MSG_CONFIRM, (const struct sockaddr*) &client_addr, len);
						if(res < 0) {
							printf("Errore nell'invio del messaggio\n");
						}	
						else{
							//printf("Messaggio al client inviato\n"); 
						}
						
						
						res = sendto(sock_fd, (const char*)d, strlen(d), MSG_CONFIRM, (const struct sockaddr*) &client_addr, len);
						if(res < 0) {
							printf("Errore nell'invio del messaggio\n");
						}	
						else{
							//printf("Messaggio al client inviato\n"); 
						}
					}
					else{
						fgets(t,200,file);
					}
				}	
			}
			printf(ANSI_COLOR_MAGENTA "*---------------------------*\n" ANSI_COLOR_RESET);
			fclose(file);
			char fine[10]="fine";
			res = sendto(sock_fd, (const char*)fine, strlen(fine), MSG_CONFIRM, (const struct sockaddr*) &client_addr, len);
			if(res < 0) {
				printf("Errore nell'invio del messaggio\n");
			}	
			else{
			//	printf("Messaggio al client inviato\n"); 
			}
		}
		else{
			if(strcmp(modalita,"lista")==0 || strcmp(modalita,"l")==0 ){
				printf("L'utente ha richiesto la lista\n");
				char* lista = lista_utenti(clientHead.first);
				printf("Questa è la lista: %s\n", lista);
				//strcpy(buf, lista);
				//memset(l,0,sizeof(l));
				char l[1024]="§";
				strcat(l,lista);
				res = sendto(sock_fd, (const char*)l, strlen(l), MSG_CONFIRM, (const struct sockaddr*) &client_addr, len);
				if(res < 0) printf("Errore nell'invio");
			}
			else{
				char* text = strtok(NULL, "]");
				char* de = strtok(NULL, " ");
				char* mi = strtok(NULL, " ");
				strcat(text, "]");
				char* t2 = (char*)malloc(1024*sizeof(char));
				memcpy(t2, text, 1024);
				int j;
				for(j=0; j<strlen(t2); j++){
					switch(t2[j]){
						case 'a': t2[j] = 'z';
						break;
						case 'b': t2[j] = 'y';
						break;
						case 'c': t2[j] = 'x';
						break;
						case 'd': t2[j] = 'w';
						break;
						case 'e': t2[j] = 'v';
						break;
						case 'f': t2[j] = 'u';
						break;
						case 'g': t2[j] = 't';
						break;
						case 'h': t2[j] = 's';
						break;
						case 'i': t2[j] = 'r';
						break;
						case 'j': t2[j] = 'q';
						break;
						case 'k': t2[j] = 'p';
						break;
						case 'l': t2[j] = 'o';
						break;
						case 'm': t2[j] = 'n';
						break;
						case 'n': t2[j] = 'm';
						break;
						case 'o': t2[j] = 'l';
						break;
						case 'p': t2[j] = 'k';
						break;
						case 'q': t2[j] = 'j';
						break;
						case 'r': t2[j] = 'i';
						break;
						case 's': t2[j] = 'h';
						break;
						case 't': t2[j] = 'g';
						break;
						case 'u': t2[j] = 'f';
						break;
						case 'v': t2[j] = 'e';
						break;
						case 'w': t2[j] = 'd';
						break;
						case 'x': t2[j] = 'c';
						break;
						case 'y': t2[j] = 'b';
						break;
						case 'z': t2[j] = 'a';
						break;
						
						case 'A': t2[j] = 'Z';
						break;
						case 'B': t2[j] = 'Y';
						break;
						case 'C': t2[j] = 'X';
						break;
						case 'D': t2[j] = 'W';
						break;
						case 'E': t2[j] = 'V';
						break;
						case 'F': t2[j] = 'U';
						break;
						case 'G': t2[j] = 'T';
						break;
						case 'H': t2[j] = 'S';
						break;
						case 'I': t2[j] = 'R';
						break;
						case 'J': t2[j] = 'Q';
						break;
						case 'K': t2[j] = 'P';
						break;
						case 'L': t2[j] = 'O';
						break;
						case 'M': t2[j] = 'N';
						break;
						case 'N': t2[j] = 'M';
						break;
						case 'O': t2[j] = 'L';
						break;
						case 'P': t2[j] = 'K';
						break;
						case 'Q': t2[j] = 'J';
						break;
						case 'R': t2[j] = 'I';
						break;
						case 'S': t2[j] = 'H';
						break;
						case 'T': t2[j] = 'G';
						break;
						case 'U': t2[j] = 'F';
						break;
						case 'V': t2[j] = 'E';
						break;
						case 'W': t2[j] = 'D';
						break;
						case 'X': t2[j] = 'C';
						break;
						case 'Y': t2[j] = 'B';
						break;
						case 'Z': t2[j] = 'A';
						break;
						
						default: ;
					}
					/*t2[j]++;*/
				}
			//	printf("Dentro text ci sta: %s, dentro t2 ci sta: %s\n", text, t2);
			//	printf("Dentro de ci sta: %s\n", de);
			//	printf("Dentro mi ci sta: %s\n", mi);
				
				len = sizeof(client_addr);
				
				int ris = ricercaD(clientHead.first, de);
			//	printf("Il risultato della ricerca è: %d\n", ris);
				if(ris == 1){			
					FILE *file = fopen("chat.txt","a");
					if(file < 0) handle_error("Errore nell'apertura del file");
					int k;
					for(k=0; k<strlen(de); k++) de[k] = toupper((unsigned char)de[k]);
					for(k=0; k<strlen(mi); k++) mi[k] = toupper((unsigned char)mi[k]);
					fprintf(file, "%s ha ricevuto da %s : %s\n", de, mi, t2);
					fclose(file);
					free(t2);
					
					
					char conferma[50];
					strcpy(conferma,"Messaggio inviato correttamente");
					res = sendto(sock_fd, (const char*)conferma, strlen(conferma), MSG_CONFIRM, (const struct sockaddr*) &client_addr, len);
					if(res < 0) {
						printf("Errore nell'invio del messaggio\n");
					}	
					else{
						printf("Messaggio al client inviato\n"); 
					}
					
				}
				else{
					char conferma[55];
					strcpy(conferma,"Spiacente, non risulta nessun utente con questo nome");
					res = sendto(sock_fd, (const char*)conferma, strlen(conferma), MSG_CONFIRM, (const struct sockaddr*) &client_addr, len);
					if(res < 0) {
						printf("Errore nell'invio del messaggio\n");
					}	
					else{
						printf("Destinatario inesistente\n"); 
					}
				}
			}
			printf(ANSI_COLOR_MAGENTA "*---------------------------*\n" ANSI_COLOR_RESET);
		}
	}
	libera_memoria(clientHead.first);
	stampa_lista(clientHead.first);
	res = close(sock_fd);
	if(res < 0) handle_error("Errore nella chiusura della socket");
	return 0;
}
