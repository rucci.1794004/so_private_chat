#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h> //htons() e inet_addr()
#include <netinet/in.h> //struct sockaddr_in
#include <sys/types.h>          
#include <sys/socket.h>
#include <signal.h>
#include <ctype.h>
#include <time.h>

#include "common.h"


int sock_fd;
char buf[1024];
char msg[1024];
int res;
char risposta[10];
char utente[15];
char mittente[15];

struct sockaddr_in server_addr;


void* lettura(struct sockaddr_in a){
	struct sockaddr_in add = a; //prendo la struct sever_addr passata dalla funzione
	printf("\n");
	while(1){
		socklen_t len = sizeof(add);
		int n = recvfrom(sock_fd, (char *)buf, sizeof(buf), MSG_WAITALL, (struct sockaddr*)&add, &len);
		//printf("Dentro n ci sta: %d\n", n);
		buf[n] = '\0';
		
		char a = buf[0];
		if(strcmp(buf,"fine")==0){
			//printf("Non ci sono nuovi messaggi da leggere\n\n");
			break;
		}
		else if(strcmp(buf,"Messaggio inviato correttamente")==0 || strcmp(buf,"Spiacente, non risulta nessun utente con questo nome")==0 || (a == -62)){}
		else{
			int j;
			for(j=0; j<strlen(buf); j++){
				switch(buf[j]){
					case 'a': buf[j] = 'z';
					break;
					case 'b': buf[j] = 'y';
					break;
					case 'c': buf[j] = 'x';
					break;
					case 'd': buf[j] = 'w';
					break;
					case 'e': buf[j] = 'v';
					break;
					case 'f': buf[j] = 'u';
					break;
					case 'g': buf[j] = 't';
					break;
					case 'h': buf[j] = 's';
					break;
					case 'i': buf[j] = 'r';
					break;
					case 'j': buf[j] = 'q';
					break;
					case 'k': buf[j] = 'p';
					break;
					case 'l': buf[j] = 'o';
					break;
					case 'm': buf[j] = 'n';
					break;
					case 'n': buf[j] = 'm';
					break;
					case 'o': buf[j] = 'l';
					break;
					case 'p': buf[j] = 'k';
					break;
					case 'q': buf[j] = 'j';
					break;
					case 'r': buf[j] = 'i';
					break;
					case 's': buf[j] = 'h';
					break;
					case 't': buf[j] = 'g';
					break;
					case 'u': buf[j] = 'f';
					break;
					case 'v': buf[j] = 'e';
					break;
					case 'w': buf[j] = 'd';
					break;
					case 'x': buf[j] = 'c';
					break;
					case 'y': buf[j] = 'b';
					break;
					case 'z': buf[j] = 'a';
					break;
					
					case 'A': buf[j] = 'Z';
					break;
					case 'B': buf[j] = 'Y';
					break;
					case 'C': buf[j] = 'X';
					break;
					case 'D': buf[j] = 'W';
					break;
					case 'E': buf[j] = 'V';
					break;
					case 'F': buf[j] = 'U';
					break;
					case 'G': buf[j] = 'T';
					break;
					case 'H': buf[j] = 'S';
					break;
					case 'I': buf[j] = 'R';
					break;
					case 'J': buf[j] = 'Q';
					break;
					case 'K': buf[j] = 'P';
					break;
					case 'L': buf[j] = 'O';
					break;
					case 'M': buf[j] = 'N';
					break;
					case 'N': buf[j] = 'M';
					break;
					case 'O': buf[j] = 'L';
					break;
					case 'P': buf[j] = 'K';
					break;
					case 'Q': buf[j] = 'J';
					break;
					case 'R': buf[j] = 'I';
					break;
					case 'S': buf[j] = 'H';
					break;
					case 'T': buf[j] = 'G';
					break;
					case 'U': buf[j] = 'F';
					break;
					case 'V': buf[j] = 'E';
					break;
					case 'W': buf[j] = 'D';
					break;
					case 'X': buf[j] = 'C';
					break;
					case 'Y': buf[j] = 'B';
					break;
					case 'Z': buf[j] = 'A';
					break;
					
					default: ;
				}
				/*buf[j]--;*/
			}
			char mod[5];
			int n = recvfrom(sock_fd, (char *)mod, 15, MSG_WAITALL, (struct sockaddr*)&add, &len);
			mod[n] = '\0';
			//questo client è il destinatario
			if(strcmp(mod,"1")==0){
			
				n = recvfrom(sock_fd, (char *)mittente, 15, MSG_WAITALL, (struct sockaddr*)&add, &len);
				mittente[n] = '\0';
				int k;
				for(k=0; k<strlen(mittente); k++) mittente[k] = toupper((unsigned char)mittente[k]);
				printf(ANSI_COLOR_BLUE "%s " ANSI_COLOR_RESET , mittente);
				printf("ha inviato: %s\n",buf);
				
			//questo client è il mittente
			}
			else if(strcmp(mod,"2")==0){
				char d[15];
				n = recvfrom(sock_fd, (char *)d, 15, MSG_WAITALL, (struct sockaddr*)&add, &len);
				d[n] = '\0';
				int k;
				for(k=0; k<strlen(d); k++) d[k] = toupper((unsigned char)d[k]);
				printf("Ho inviato a ");
				printf(ANSI_COLOR_GREEN "%s " ANSI_COLOR_RESET , d	);
				printf(": %s\n",buf);
			}
		memset(buf, 0, strlen(buf));
		memset(mittente, 0, strlen(mittente));
		}
	}
	return 0;
}

void scrittura(struct sockaddr_in a){
	char destinatario[15];
	
	struct sockaddr_in add = a; //prendo la struct sever_addr passata dalla funzione
	printf("Scrivi messaggio da inviare: ");
	fgets(msg, 1024, stdin);
	
	size_t ln = strlen(msg) - 1;
	if (*msg && msg[ln] == '\n') 
	msg[ln] = '\0';
    
    while(strncmp(msg,"login",5)==0){
		printf("Non puoi effettuare nuovamente il login se prima non ti disconnetti\n");
		printf("Scrivi messaggio da inviare: ");
		fgets(msg, 1024, stdin);
	}
	ln = strlen(msg) - 1;
	if (*msg && msg[ln] == '\n') 
	msg[ln] = '\0';
	//printf("Messaggio salvato, hai scritto: %s",msg);
	
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	char giorno [6];
	char mese [6];
	char anno [6];
	char ora [6];
	char minuto [6];
	char secondo [6];
	char data[] = "[";
	
	snprintf (giorno, sizeof(giorno), "%d", tm.tm_mday);
	strcat(data,giorno);
	strcat(data, "-");
	
	snprintf (mese, sizeof(mese), "%d", tm.tm_mon + 1);
	strcat(data,mese);
	strcat(data, "-");
	
	snprintf (anno, sizeof(anno), "%d", tm.tm_year + 1900);
	strcat(data,anno);
	strcat(data, " ");
	
	snprintf (ora, sizeof(ora), "%d", tm.tm_hour);
	strcat(data,ora);
	strcat(data, ":");
	
	snprintf (minuto, sizeof(minuto), "%d", tm.tm_min);
	strcat(data,minuto);
	strcat(data, ":");
	
	snprintf (secondo, sizeof(secondo), "%d", tm.tm_sec);
	strcat(data,secondo);
	strcat(data,"]");
	
	strcat(msg, " ");
	strcat(msg, data);
    
    char messaggio[1024] = "messaggio ";
    strcat(messaggio,msg);
    strcat(messaggio, " ");
    
    while(1){
		printf("Specificare destinatario messaggio: ");
		scanf("%s", destinatario);
		while('\n'!=getchar());
		if(strcmp(destinatario,utente) == 0) printf(ANSI_COLOR_RED "Non è possibile inviare un messaggio a se stessi\n\n" ANSI_COLOR_RESET);
		else break;
	}
	strcat(messaggio,destinatario);
    strcat(messaggio, " ");
	
	strcat(messaggio,utente);
	//printf("Dentro messaggio ci sta: %s\n", messaggio);
	
	res = sendto(sock_fd, (const char *)messaggio, strlen(messaggio), MSG_CONFIRM, (const struct sockaddr*) &add, sizeof(add));	
	if(res < 0) handle_error("Errore nell'invio\n");
	
	//char* modalita = strtok(messaggio, " ");
	
	
	socklen_t len = sizeof(add);
	char mex[1024];
	
//	while(strcmp(mex,"Spiacente, non risulta nessun utente con questo nome")!=0){
	int n = recvfrom(sock_fd, (char *)mex, 1024, MSG_WAITALL, (struct sockaddr*)&add, &len);
	mex[n] = '\0';
//		if(strcmp(mex,"Messaggio inviato correttamente")==0) break;
//	}

	if(strcmp(mex,"Messaggio inviato correttamente")==0)
		printf(ANSI_COLOR_YELLOW "%s\n" ANSI_COLOR_RESET ,mex);
	else if(strcmp(mex,"Spiacente, non risulta nessun utente con questo nome")==0)
		printf(ANSI_COLOR_RED "%s\n" ANSI_COLOR_RESET ,mex);
	
	memset(msg,0,strlen(msg));
	memset(destinatario,0,strlen(destinatario));
}

//gestione segnale SIGINT
void chiusura(){
	//printf("\nHai premuto ctrl+c\n");
	printf("\n\n");
	printf(ANSI_COLOR_YELLOW "Vuoi veramente uscire? " ANSI_COLOR_RESET);
	
	E:scanf("%s", risposta);
	while('\n'!=getchar());
	if(strcmp(risposta,"si")==0 || strcmp(risposta, "SI") == 0){
		printf("Esco dal programma..\n");
		res = close(sock_fd);
		if(res < 0) handle_error("Errore nella chiusura della socket");
		
		exit(0);
	}
	else if(strcmp(risposta,"no")==0 || strcmp(risposta, "NO") == 0){
		printf("Ok, continuiamo\n");
	}
	else{
		printf("Devi rispondere si o no: ");
		goto E;
	}
}

int main(){
	signal(SIGINT, chiusura);
	signal(SIGQUIT, chiusura);
	
	sock_fd = socket(AF_INET, SOCK_DGRAM, 0); //creo la socket
	if(sock_fd < 0) handle_error("Errore nella creazione della socket");
	
	//struct sockaddr_in server_addr; //dichiaro la struct sockaddr_in
	memset(&server_addr, 0, sizeof(server_addr)); //inizializzo a 0 tutti i campi
	
	//int port = 8080;
	
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(PORT);
	//server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_addr.s_addr = inet_addr(SERVER_ADDRESS);
	
	
	size_t buf_len = sizeof(buf); //lunghezza buf
    memset(buf,0,buf_len);
	
	printf(ANSI_COLOR_YELLOW "**** AVVIO CLIENT **** \n" ANSI_COLOR_RESET);
	
	
	while(1){
		printf("Devi effettuare il login\n");
		char l[1024] = "login ";
		
		printf("Scegli il nome utente con cui vuoi effettuare il login: ");
		scanf("%s", utente);
		while('\n'!=getchar());
		strcat(l, utente);
		strcat(l, " ");
		
		char password[15];
		printf("Inserire la password: ");
		scanf("%s", password);
		while('\n'!=getchar());
		strcat(l, password);
		
		res = sendto(sock_fd, (const char *)l, sizeof(l), MSG_CONFIRM, (const struct sockaddr*) &server_addr, sizeof(server_addr));
		if(res < 0) handle_error("Errore nell'invio del messaggio");
		
		socklen_t len = sizeof(server_addr);
		char response[15];
		//memset(response,0,15);
		int n = recvfrom(sock_fd, (char *)response, sizeof(response), MSG_WAITALL, (struct sockaddr*)&server_addr, &len);
		response[n] = '\0';
		if(strcmp(response,"no")==0) printf(ANSI_COLOR_RED "Spiacente, nome utente già in uso. Impossibile effettuare il login si prega di provare con un nome diverso\n\n" ANSI_COLOR_RESET);
		else{
			printf("Login effettuato con successo\n\n");
			printf(ANSI_COLOR_YELLOW "**** Benvenuto/a %s ****\n" ANSI_COLOR_RESET, utente);
			break;
		 }

	}
	
	
	
    while(1){
		
	//	pthread_t thread;
	//	pthread_create(&thread, NULL, aa, NULL);
		//invio messaggio
		printf(ANSI_COLOR_CYAN "Vuoi scrivere un messaggio, leggere i messaggi oppure vuoi uscire? " ANSI_COLOR_RESET);
		scanf("%s", msg);
		while('\n'!=getchar());
		
		if(strcmp(msg,"s")==0 || strcmp(msg,"scrivi")==0 || strcmp(msg,"SCRIVI")==0){
			
			printf("Vuoi inviare un messaggio o vuoi vedere la lista degli utenti?\n");
			printf("Scrivi LISTA o MESSAGGIO: ");
			//char msg[1024];
	
			scanf("%s", msg);
			while('\n'!=getchar());
	
			if(strcmp(msg,"LISTA")==0 || strcmp(msg,"lista")==0 || strcmp(msg,"l")==0) {
				printf("Hai richiesto la lista.\n");
				char lista[1024] = "lista";
				res = sendto(sock_fd, (const char *)lista, strlen(lista), MSG_CONFIRM, (const struct sockaddr*) &server_addr, sizeof(server_addr));
				if(res < 0) handle_error("Errore nell'invio del messaggio");
				//printf("Messaggio inviato correttamente\n");
				
				memset(buf,0,sizeof(buf));
				socklen_t len = sizeof(server_addr);
				
				int n = recvfrom(sock_fd, (char *)buf, buf_len, MSG_WAITALL, (struct sockaddr*)&server_addr, &len);
				buf[n] = '\0';
				
				memmove (buf, buf+2, strlen (buf+1) + 1);
				printf("Gli utenti registrati sono: %s\n", buf);
			}
	
			else if(strcmp(msg,"MESSAGGIO")==0 || strcmp(msg,"messaggio" )==0 || strcmp(msg,"m")==0){
				scrittura(server_addr);
			}
	
			else{
				printf("Puoi scrivere solo LISTA, MESSAGGIO\n");
			}
			printf("\n");
		}
		else if(strcmp(msg,"l")==0 || strcmp(msg,"leggi")==0 || strcmp(msg,"LEGGI")==0){
			char leggo[100] = "leggo ";
			strcat(leggo, utente);
			res = sendto(sock_fd, (const char *)leggo, strlen(leggo), MSG_CONFIRM, (const struct sockaddr*) &server_addr, sizeof(server_addr));	
			if(res<0) handle_error("Errore nell'invio del messaggio\n");
			lettura(server_addr);
		}
		else if(strcmp(msg,"q")==0){
			printf(ANSI_COLOR_YELLOW "Sei sicuro di voler uscire? " ANSI_COLOR_RESET);
			scanf("%s", risposta);
			while('\n'!=getchar());
			if(strcmp(risposta,"si")==0 || strcmp(risposta, "SI") == 0)
				break;
		}
		else{
			printf("Puoi digitare solo SCRIVI, LEGGI o QUIT\n");
		}
	}
	printf("Esco dal programma..\n");
	res = close(sock_fd);
	if(res < 0) handle_error("Errore nella chiusura della socket\n");
	return 0;
}
