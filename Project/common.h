#ifndef COMMON_H
#define COMMON_H

#define handle_error_en(en,msg) do {errno = en; perro(msg); exit(EXIT_FAILURE);} while(0)
#define handle_error(msg) do{ perror(msg); exit(EXIT_FAILURE);} while(0)

#endif

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define PORT 8080
#define SERVER_ADDRESS "79.18.140.52"

typedef struct client {
	char* username;
	char* password;
	struct sockaddr_in utente;
	struct client* next;
} client;

typedef struct head{
	struct client* first;
}head;

//server
void libera_memoria(struct client* head);
void chiusura();
void inserisci_utente(char* buf, struct sockaddr_in client_addr, char* pass);
void ripeti_login(char* buf, struct sockaddr_in client_addr, struct client*head ,char* pass);
void stampa_lista(struct client* head);
void libera_memoria(struct client* head);
char* lista_utenti(struct client* head);
int ricerca(struct client* head, char*dest, char* pass);
//	struct sockaddr_in cerca_destinatario(struct client* head, char* dest);
int ricercaD(struct client* head, char*dest);

//client
void* lettura(struct sockaddr_in a);
void scrittura(struct sockaddr_in a);
